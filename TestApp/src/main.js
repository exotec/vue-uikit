import { createApp } from 'vue'
import {VueUIkit} from '@exotec/vue-uikit'
import App from './App.vue'

const app = createApp(App)
app.use(VueUIkit)
app.mount('#app')
