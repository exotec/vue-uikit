'use strict';

var vue = require('vue');

/**
 * @group Accordion
 * Create a list of items that can be shown individually by clicking an item's header.
 * <br /><br />
 * `<uk-accordion :items="items"
 *                     :multiple="multiple"
 *                     :collapsible="collapsible"
 *                     :transition="transition"
 *                     :duration="duration"/>`
 */
var script$e = {
  name: "uk-accordion",
  props: {
    // Array of item objects<br />
    // [@see &lt;uk-accordion-item&gt;](/components/uk-accordion-item).
    items: {
      type: Array,
      required: true
    },
    // Allow multiple open items
    multiple: {
      type: Boolean,
      required: false,
      default: false
    },
    // Allow all items to be closed
    collapsible: {
      type: Boolean,
      required: false,
      default: true
    },
    // The transition to use when revealing items. Use keyword for [@see easing functions](https://developer.mozilla.org/en-US/docs/Web/CSS/easing-function#values)
    transition: {
      type: String,
      required: false,
      default: 'ease'
    },
    // Animation duration in milliseconds
    duration: {
      type: Number,
      required: false,
      default: 200
    },
    // Index of the element to open initially
    active: {
      type: Number,
      required: false,
    }
  }
};

const _hoisted_1$e = ["data-uk-accordion"];

function render$e(_ctx, _cache, $props, $setup, $data, $options) {
  return (vue.openBlock(), vue.createElementBlock("ul", {
    "data-uk-accordion": `multiple: ${$props.multiple};  collapsible: ${$props.collapsible}; transition: ${$props.transition}; duration: ${$props.duration}; ${$props.active !== false ? `active: ${$props.active};` : ''}`
  }, [
    (vue.openBlock(true), vue.createElementBlock(vue.Fragment, null, vue.renderList($props.items, (item, key) => {
      return (vue.openBlock(), vue.createBlock(vue.resolveDynamicComponent("uk-accordion-item"), {
        title: item.title,
        class: vue.normalizeClass(item.class),
        content: item.content,
        open: item.open,
        key: key
      }, null, 8 /* PROPS */, ["title", "class", "content", "open"]))
    }), 128 /* KEYED_FRAGMENT */))
  ], 8 /* PROPS */, _hoisted_1$e))
}

script$e.render = render$e;
script$e.__file = "src/components/Accordion/Base.vue";

/**
 * @group Accordion
 * The item of accordion
 * <br /><br />
 * `   <uk-accordion-item :title="item.title" :class="item.class" :content="item.content" :open="item.open"/>`
 */
var script$d = {
  name: "uk-accordion-item",
  props: {
    // The title of the item
    title: {
      type: String,
      required: true
    },
    // The CSS class of the item
    class: {
      type: String,
      required: false
    },
    // The HTML content of the item
    content: {
      type: String,
      required: true
    },
    // Whether the item is open or not
    open: {
      type: Boolean,
      required: false
    }
  }
};

const _hoisted_1$d = {
  class: "uk-accordion-title",
  href: "#"
};
const _hoisted_2$a = ["innerHTML"];

function render$d(_ctx, _cache, $props, $setup, $data, $options) {
  return (vue.openBlock(), vue.createElementBlock("li", {
    class: vue.normalizeClass(`${$props.open ? 'uk-open' : ''} ${this.class}`)
  }, [
    vue.createElementVNode("a", _hoisted_1$d, vue.toDisplayString($props.title), 1 /* TEXT */),
    vue.createElementVNode("div", {
      class: "uk-accordion-content",
      innerHTML: $props.content
    }, null, 8 /* PROPS */, _hoisted_2$a)
  ], 2 /* CLASS */))
}

script$d.render = render$d;
script$d.__file = "src/components/Accordion/Item.vue";

/**
 * @group Modal
 */
var script$c = {
  name: 'uk-modal',
  props: {
    modal: {
      type: Object,
      required: true,
    }
  }
};

const _hoisted_1$c = { "data-uk-modal": "" };
const _hoisted_2$9 = {
  key: 0,
  class: "uk-modal-close-default",
  type: "button",
  "data-uk-close": ""
};

function render$c(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_uk_modal_header = vue.resolveComponent("uk-modal-header");
  const _component_uk_modal_body = vue.resolveComponent("uk-modal-body");
  const _component_uk_modal_footer = vue.resolveComponent("uk-modal-footer");

  return (vue.openBlock(), vue.createElementBlock("div", _hoisted_1$c, [
    vue.createElementVNode("div", {
      class: vue.normalizeClass(`uk-modal-dialog ${$props.modal.center ? 'uk-margin-auto-vertical' : ''}`)
    }, [
      ($props.modal.closeButton)
        ? (vue.openBlock(), vue.createElementBlock("button", _hoisted_2$9))
        : vue.createCommentVNode("v-if", true),
      vue.createVNode(_component_uk_modal_header, {
        content: $props.modal.header
      }, null, 8 /* PROPS */, ["content"]),
      vue.createVNode(_component_uk_modal_body, {
        content: $props.modal.body
      }, null, 8 /* PROPS */, ["content"]),
      vue.createVNode(_component_uk_modal_footer, {
        content: $props.modal.footer
      }, null, 8 /* PROPS */, ["content"])
    ], 2 /* CLASS */)
  ]))
}

script$c.render = render$c;
script$c.__file = "src/components/Modal/Base.vue";

/**
 * @group Modal
 */
var script$b = {
  name: 'uk-modal-header',
  props: {
    content: {
      type: Object,
      required: false
    }
  }
};

const _hoisted_1$b = ["innerHTML"];

function render$b(_ctx, _cache, $props, $setup, $data, $options) {
  return (vue.openBlock(), vue.createElementBlock("div", {
    class: "uk-modal-header",
    innerHTML: $props.content.html
  }, null, 8 /* PROPS */, _hoisted_1$b))
}

script$b.render = render$b;
script$b.__file = "src/components/Modal/Header.vue";

/**
 * @group Modal
 */
var script$a = {
  name: 'uk-modal-body',
  props: {
    content: {
      type: Object,
      required: true
    }
  }
};

const _hoisted_1$a = ["innerHTML"];

function render$a(_ctx, _cache, $props, $setup, $data, $options) {
  return (vue.openBlock(), vue.createElementBlock("div", {
    class: "uk-modal-body",
    innerHTML: $props.content.html
  }, null, 8 /* PROPS */, _hoisted_1$a))
}

script$a.render = render$a;
script$a.__file = "src/components/Modal/Body.vue";

/**
 * @group Modal
 */
var script$9 = {
  name: 'uk-modal-footer',
  props: {
    content: {
      type: Object,
      required: true
    }
  }
};

const _hoisted_1$9 = { class: "uk-modal-footer" };
const _hoisted_2$8 = ["innerHTML"];
const _hoisted_3$6 = {
  key: 1,
  class: "uk-text-right"
};

function render$9(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_uk_button = vue.resolveComponent("uk-button");

  return (vue.openBlock(), vue.createElementBlock("div", _hoisted_1$9, [
    ($props.content.html)
      ? (vue.openBlock(), vue.createElementBlock("div", {
          key: 0,
          innerHTML: $props.content.html
        }, null, 8 /* PROPS */, _hoisted_2$8))
      : vue.createCommentVNode("v-if", true),
    ($props.content.buttons)
      ? (vue.openBlock(), vue.createElementBlock("div", _hoisted_3$6, [
          (vue.openBlock(true), vue.createElementBlock(vue.Fragment, null, vue.renderList($props.content.buttons, (button) => {
            return (vue.openBlock(), vue.createBlock(_component_uk_button, {
              button: button,
              onClick: button.onClick
            }, null, 8 /* PROPS */, ["button", "onClick"]))
          }), 256 /* UNKEYED_FRAGMENT */))
        ]))
      : vue.createCommentVNode("v-if", true)
  ]))
}

script$9.render = render$9;
script$9.__file = "src/components/Modal/Footer.vue";

/**
 * @group Alert
 * Display success, warning and error messages.
 * <br /><br />
 * `<uk-alert :class="class" :content="html" :animation="animation" :duration:="duration"/>`
 */
var script$8 = {
  name: 'uk-alert',
  props: {
    // The HTML content of the alert
    content: {
      type: String,
      required: true
    },
    closable: {
      type: Boolean,
      required: false,
      default: true
    },
    largeCloseIcon: {
      type: Boolean,
      required: false,
      default: false
    },
    // The class of the alert
    class: {
      type: String,
      required: false
    },
    // Fade out or hide directly
    animation: {
      type: Boolean,
      required: false,
      default: true
    },
    // Animation duration in milliseconds.
    duration: {
      type: Number,
      required: false,
      default: 150
    },
  }
};

const _hoisted_1$8 = ["data-uk-alert"];
const _hoisted_2$7 = ["innerHTML"];

function render$8(_ctx, _cache, $props, $setup, $data, $options) {
  return (vue.openBlock(), vue.createElementBlock("div", {
    "data-uk-alert": `animation: ${$props.animation}; duration: ${$props.duration};`,
    class: vue.normalizeClass(`uk-alert uk-alert-${this.class}`)
  }, [
    ($props.closable)
      ? (vue.openBlock(), vue.createElementBlock("a", {
          key: 0,
          class: vue.normalizeClass(`uk-alert-close ${$props.largeCloseIcon ? 'uk-close-large' : ''}`),
          "data-uk-close": ""
        }, null, 2 /* CLASS */))
      : vue.createCommentVNode("v-if", true),
    vue.createElementVNode("div", { innerHTML: $props.content }, null, 8 /* PROPS */, _hoisted_2$7)
  ], 10 /* CLASS, PROPS */, _hoisted_1$8))
}

script$8.render = render$8;
script$8.__file = "src/components/Alert/Base.vue";

var script$7 = {
  name: 'uk-slider',
  props: {
    slider: {
      type: Object,
      required: true
    }
  }
};

const _hoisted_1$7 = {
  class: "uk-position-relative uk-visible-toggle uk-light",
  tabindex: "-1",
  "data-uk-slider": ""
};
const _hoisted_2$6 = { class: "uk-slider-items uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-4@m" };
const _hoisted_3$5 = ["src"];
const _hoisted_4$3 = { class: "uk-position-center uk-panel" };
const _hoisted_5$2 = /*#__PURE__*/vue.createElementVNode("a", {
  class: "uk-position-center-left uk-position-small uk-hidden-hover",
  href: "#",
  "uk-slidenav-previous": "",
  "uk-slider-item": "previous"
}, null, -1 /* HOISTED */);
const _hoisted_6$1 = /*#__PURE__*/vue.createElementVNode("a", {
  class: "uk-position-center-right uk-position-small uk-hidden-hover",
  href: "#",
  "uk-slidenav-next": "",
  "uk-slider-item": "next"
}, null, -1 /* HOISTED */);

function render$7(_ctx, _cache, $props, $setup, $data, $options) {
  return (vue.openBlock(), vue.createElementBlock("div", _hoisted_1$7, [
    vue.createElementVNode("ul", _hoisted_2$6, [
      (vue.openBlock(true), vue.createElementBlock(vue.Fragment, null, vue.renderList($props.slider.items, (item, key) => {
        return (vue.openBlock(), vue.createElementBlock("li", { key: key }, [
          vue.createElementVNode("img", {
            src: item.img,
            width: "400",
            height: "600",
            alt: ""
          }, null, 8 /* PROPS */, _hoisted_3$5),
          vue.createElementVNode("div", _hoisted_4$3, [
            vue.createElementVNode("h1", null, "Key: " + vue.toDisplayString(key), 1 /* TEXT */)
          ])
        ]))
      }), 128 /* KEYED_FRAGMENT */))
    ]),
    _hoisted_5$2,
    _hoisted_6$1
  ]))
}

script$7.render = render$7;
script$7.__file = "src/components/Slider/Base.vue";

/**
 * @group Form
 * Easily create nice looking buttons, which come in different styles.
 * <br /><br />
 * `<uk-button :label="label"
 *                  :class="class"
 *                  :size="size"
 *                  :name="name"
 *                  :icon="icon"
 *                  :iconPosition="iconPosition"
 *                  :disabled="disabled"/>`
 */
var script$6 = {
  name: "uk-button",
  props: {
    // The label of the button
    label: {
      type: String,
      required: true
    },
    // The CSS class of the button
    class: {
      type: String,
      required: false
    },
    // The name of the button
    name: {
      type: String,
      required: false,
      default: "button"
    },
    // Disable the button
    disabled: {
      type: Boolean,
      required: false,
      default: false
    },
    // The icon of the button
    icon: {
      type: String,
      required: false
    },
    // The icon position
    iconPosition: {
      type: String,
      required: false,
      default: "left"
    },
    // The size of the button
    size: {
      type: String,
      required: false,
    }
  }
};

const _hoisted_1$6 = ["name", "disabled"];
const _hoisted_2$5 = ["data-uk-icon"];
const _hoisted_3$4 = ["data-uk-icon"];

function render$6(_ctx, _cache, $props, $setup, $data, $options) {
  return (vue.openBlock(), vue.createElementBlock("button", {
    class: vue.normalizeClass(`uk-button uk-button-${this.class} uk-button-${$props.size}`),
    name: $props.name,
    disabled: !!$props.disabled,
    onClick: _cache[0] || (_cache[0] = (...args) => (_ctx.onClick && _ctx.onClick(...args)))
  }, [
    ($props.icon && $props.iconPosition === 'left')
      ? (vue.openBlock(), vue.createElementBlock("span", {
          key: 0,
          class: "uk-margin-small-right",
          "data-uk-icon": `icon: ${$props.icon}`
        }, null, 8 /* PROPS */, _hoisted_2$5))
      : vue.createCommentVNode("v-if", true),
    vue.createTextVNode(" " + vue.toDisplayString($props.label) + " ", 1 /* TEXT */),
    ($props.icon && $props.iconPosition === 'right')
      ? (vue.openBlock(), vue.createElementBlock("span", {
          key: 1,
          class: "uk-margin-small-left",
          "data-uk-icon": `icon: ${$props.icon}`
        }, null, 8 /* PROPS */, _hoisted_3$4))
      : vue.createCommentVNode("v-if", true)
  ], 10 /* CLASS, PROPS */, _hoisted_1$6))
}

script$6.render = render$6;
script$6.__file = "src/components/Form/Button/Base.vue";

var script$5 = {
  name: "uk-input-text",
  props: {
    input: {
      type: Object,
      required: false,
    },
  },
  created() {
    this.input.class = `${this.colorClass} ${this.sizeClass} ${this.widthClass} ${this.blankClass}`;
  },
  computed: {
    colorClass() {
      return this.input.class ? `uk-input uk-form-${this.input.class}` : 'uk-input';
    },
    sizeClass() {
      return this.input.size ? `uk-form-${this.input.size}` : '';
    },
    widthClass() {
      return this.input.width ? `uk-width-${this.input.width}` : '';
    },
    blankClass() {
      return this.input.blank ? `uk-form-blank` : '';
    },
  },
};

const _hoisted_1$5 = {
  key: 0,
  class: "uk-inline"
};
const _hoisted_2$4 = ["data-uk-icon"];
const _hoisted_3$3 = ["type", "id", "placeholder", "value", "disabled", "readonly", "required"];
const _hoisted_4$2 = ["type", "id", "placeholder", "value", "disabled", "readonly", "required"];

function render$5(_ctx, _cache, $props, $setup, $data, $options) {
  return ($props.input.icon)
    ? (vue.openBlock(), vue.createElementBlock("div", _hoisted_1$5, [
        vue.createElementVNode("span", {
          class: vue.normalizeClass(`uk-form-icon ${$props.input.iconPosition === 'right' ? 'uk-form-icon-flip' : ''}`),
          "data-uk-icon": `icon: ${$props.input.icon}`
        }, null, 10 /* CLASS, PROPS */, _hoisted_2$4),
        vue.createElementVNode("input", {
          type: $props.input.type,
          class: vue.normalizeClass($props.input.class),
          id: $props.input.id,
          placeholder: $props.input.placeholder,
          value: $props.input.value,
          disabled: !!$props.input.disabled,
          readonly: !!$props.input.readOnly,
          required: !!$props.input.required
        }, null, 10 /* CLASS, PROPS */, _hoisted_3$3)
      ]))
    : (vue.openBlock(), vue.createElementBlock("input", {
        key: 1,
        type: $props.input.type,
        class: vue.normalizeClass($props.input.class),
        id: $props.input.id,
        placeholder: $props.input.placeholder,
        value: $props.input.value,
        disabled: !!$props.input.disabled,
        readonly: !!$props.input.readOnly,
        required: !!$props.input.required
      }, null, 10 /* CLASS, PROPS */, _hoisted_4$2))
}

script$5.render = render$5;
script$5.__file = "src/components/Form/Input/Text.vue";

/**
 * @group Form
 * Easily create nice looking buttons, which come in different styles.
 * <br /><br />
 * `<uk-button :label="label"
 *                  :class="class"
 *                  :size="size"
 *                  :name="name"
 *                  :icon="icon"
 *                  :iconPosition="iconPosition"
 *                  :disabled="disabled"/>`
 */

var script$4 = {
  name: 'uk-input',
  components: {
    Field: script$5
  },
  props: {
    input: {
      type: Object,
      required: true,
    }
  }
};

const _hoisted_1$4 = ["for"];

function render$4(_ctx, _cache, $props, $setup, $data, $options) {
  return (vue.openBlock(), vue.createElementBlock(vue.Fragment, null, [
    vue.createElementVNode("label", {
      class: "uk-form-label",
      for: $props.input.id
    }, vue.toDisplayString($props.input.label), 9 /* TEXT, PROPS */, _hoisted_1$4),
    (vue.openBlock(), vue.createBlock(vue.resolveDynamicComponent(`uk-input-${$props.input.type}`), { input: $props.input }, null, 8 /* PROPS */, ["input"]))
  ], 64 /* STABLE_FRAGMENT */))
}

script$4.render = render$4;
script$4.__file = "src/components/Form/Input/Base.vue";

var script$3 = {
  name: "uk-select",
  props: {
    select: {
      type: Object,
      required: true
    }
  }
};

const _hoisted_1$3 = { class: "uk-form-controls" };
const _hoisted_2$3 = ["name", "id", "multiple"];
const _hoisted_3$2 = ["value", "selected"];

function render$3(_ctx, _cache, $props, $setup, $data, $options) {
  return (vue.openBlock(), vue.createElementBlock("div", _hoisted_1$3, [
    vue.createElementVNode("select", {
      class: vue.normalizeClass(`uk-select ${$props.select.class}`),
      name: $props.select.name,
      id: $props.select.id,
      multiple: $props.select.multiple
    }, [
      (vue.openBlock(true), vue.createElementBlock(vue.Fragment, null, vue.renderList($props.select.items, (item, key) => {
        return (vue.openBlock(), vue.createElementBlock("option", {
          key: key,
          value: item.value,
          selected: item.selected
        }, vue.toDisplayString(item.label), 9 /* TEXT, PROPS */, _hoisted_3$2))
      }), 128 /* KEYED_FRAGMENT */))
    ], 10 /* CLASS, PROPS */, _hoisted_2$3)
  ]))
}

script$3.render = render$3;
script$3.__file = "src/components/Form/Select/Base.vue";

var script$2 = {
  name: "uk-menu-list",
  props: {
    items: {
      type: Array,
      required: true
    }
  }
};

const _hoisted_1$2 = { class: "uk-navbar-nav" };
const _hoisted_2$2 = ["href"];
const _hoisted_3$1 = {
  key: 0,
  class: "uk-navbar-dropdown"
};
const _hoisted_4$1 = { class: "uk-nav uk-navbar-dropdown-nav" };
const _hoisted_5$1 = ["href"];

function render$2(_ctx, _cache, $props, $setup, $data, $options) {
  return (vue.openBlock(), vue.createElementBlock("ul", _hoisted_1$2, [
    (vue.openBlock(true), vue.createElementBlock(vue.Fragment, null, vue.renderList($props.items, (item) => {
      return (vue.openBlock(), vue.createElementBlock("li", {
        key: item.id,
        class: vue.normalizeClass(item.active ? 'uk-active' : '')
      }, [
        vue.createElementVNode("a", {
          href: item.url
        }, vue.toDisplayString(item.title), 9 /* TEXT, PROPS */, _hoisted_2$2),
        (item.children)
          ? (vue.openBlock(), vue.createElementBlock("div", _hoisted_3$1, [
              vue.createElementVNode("ul", _hoisted_4$1, [
                (vue.openBlock(true), vue.createElementBlock(vue.Fragment, null, vue.renderList(item.children, (item) => {
                  return (vue.openBlock(), vue.createElementBlock("li", {
                    key: item.id,
                    class: vue.normalizeClass(item.active ? 'uk-active' : '')
                  }, [
                    vue.createElementVNode("a", {
                      href: item.url
                    }, vue.toDisplayString(item.title), 9 /* TEXT, PROPS */, _hoisted_5$1)
                  ], 2 /* CLASS */))
                }), 128 /* KEYED_FRAGMENT */))
              ])
            ]))
          : vue.createCommentVNode("v-if", true)
      ], 2 /* CLASS */))
    }), 128 /* KEYED_FRAGMENT */))
  ]))
}

script$2.render = render$2;
script$2.__file = "src/components/Navbar/MenuList.vue";

var script$1 = {
  name: "uk-navbar-logo",
 props: {
    logo: {
      type: Object,
      required: true
    }
  }
};

const _hoisted_1$1 = ["href"];
const _hoisted_2$1 = ["src", "alt", "title", "width", "height"];

function render$1(_ctx, _cache, $props, $setup, $data, $options) {
  return (vue.openBlock(), vue.createElementBlock("a", {
    href: $props.logo.href,
    class: "uk-navbar-item uk-logo"
  }, [
    vue.createElementVNode("img", {
      src: $props.logo.src,
      alt: $props.logo.alternative,
      title: $props.logo.title,
      width: $props.logo.width,
      height: $props.logo.height
    }, null, 8 /* PROPS */, _hoisted_2$1)
  ], 8 /* PROPS */, _hoisted_1$1))
}

script$1.render = render$1;
script$1.__file = "src/components/Navbar/Logo.vue";

var script = {
  name: 'uk-navbar',
  components: {UkNavbarLogo: script$1, UkMenuList: script$2},
  props: {
    items: {
      type: Object,
      required: true
    },
    logo: {
      type: Object,
      required: false
    },
    position: {
      type: String,
      required: false,
      default: 'left'
    }
  },
};

const _hoisted_1 = {
  class: "uk-navbar-container",
  "data-uk-navbar": ""
};
const _hoisted_2 = {
  key: 0,
  class: "uk-navbar-left"
};
const _hoisted_3 = {
  key: 1,
  class: "uk-navbar-right"
};
const _hoisted_4 = {
  key: 2,
  class: "uk-navbar-left"
};
const _hoisted_5 = {
  key: 3,
  class: "uk-navbar-right"
};
const _hoisted_6 = {
  key: 4,
  class: "uk-navbar-center"
};

function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_uk_menu_list = vue.resolveComponent("uk-menu-list");
  const _component_uk_navbar_logo = vue.resolveComponent("uk-navbar-logo");

  return (vue.openBlock(), vue.createElementBlock("nav", _hoisted_1, [
    ($props.position === 'left')
      ? (vue.openBlock(), vue.createElementBlock("div", _hoisted_2, [
          vue.createVNode(_component_uk_menu_list, { items: $props.items }, null, 8 /* PROPS */, ["items"])
        ]))
      : vue.createCommentVNode("v-if", true),
    ($props.position === 'left')
      ? (vue.openBlock(), vue.createElementBlock("div", _hoisted_3, [
          ($props.logo)
            ? (vue.openBlock(), vue.createBlock(_component_uk_navbar_logo, {
                key: 0,
                logo: $props.logo
              }, null, 8 /* PROPS */, ["logo"]))
            : vue.createCommentVNode("v-if", true)
        ]))
      : vue.createCommentVNode("v-if", true),
    ($props.position === 'right')
      ? (vue.openBlock(), vue.createElementBlock("div", _hoisted_4, [
          ($props.logo)
            ? (vue.openBlock(), vue.createBlock(_component_uk_navbar_logo, {
                key: 0,
                logo: $props.logo
              }, null, 8 /* PROPS */, ["logo"]))
            : vue.createCommentVNode("v-if", true)
        ]))
      : vue.createCommentVNode("v-if", true),
    ($props.position === 'right')
      ? (vue.openBlock(), vue.createElementBlock("div", _hoisted_5, [
          vue.createVNode(_component_uk_menu_list, { items: $props.items }, null, 8 /* PROPS */, ["items"])
        ]))
      : vue.createCommentVNode("v-if", true),
    ($props.position === 'center')
      ? (vue.openBlock(), vue.createElementBlock("div", _hoisted_6, [
          ($props.logo && $props.logo.position === 'left')
            ? (vue.openBlock(), vue.createBlock(_component_uk_navbar_logo, {
                key: 0,
                logo: $props.logo
              }, null, 8 /* PROPS */, ["logo"]))
            : vue.createCommentVNode("v-if", true),
          vue.createVNode(_component_uk_menu_list, { items: $props.items }, null, 8 /* PROPS */, ["items"]),
          ($props.logo && $props.logo.position === 'right')
            ? (vue.openBlock(), vue.createBlock(_component_uk_navbar_logo, {
                key: 1,
                logo: $props.logo
              }, null, 8 /* PROPS */, ["logo"]))
            : vue.createCommentVNode("v-if", true)
        ]))
      : vue.createCommentVNode("v-if", true)
  ]))
}

script.render = render;
script.__file = "src/components/Navbar/Base.vue";

var components = {
    AccordionBase: script$e,
    AccordionItem: script$d,
    ModalBase: script$c,
    ModalHeader: script$b,
    ModalBody: script$a,
    ModalFooter: script$9,
    AlertBase: script$8,
    SliderBase: script$7,
    FormButtonBase: script$6,
    FormInputBase: script$4,
    FormInputText: script$5,
    FormSelectBase: script$3,
    NavbarBase: script
};

const plugin = {
    install (Vue) {
        for (const prop in components) {
            if (components.hasOwnProperty(prop)) {
                const component = components[prop];
                Vue.component(component.name, component);
            }
        }
    }
};

const VueUIkit = plugin;

exports.VueUIkit = VueUIkit;
