import AccordionBase from './components/Accordion/Base.vue';
import AccordionItem from './components/Accordion/Item.vue';

import ModalBase from './components/Modal/Base.vue';
import ModalHeader from './components/Modal/Header.vue';
import ModalBody from './components/Modal/Body.vue';
import ModalFooter from './components/Modal/Footer.vue';
import AlertBase from './components/Alert/Base.vue';
import SliderBase from './components/Slider/Base.vue';

import FormButtonBase from './components/Form/Button/Base.vue';

import FormInputBase from './components/Form/Input/Base.vue';
import FormInputText from './components/Form/Input/Text.vue';

import FormSelectBase from './components/Form/Select/Base.vue';

import NavbarBase from './components/Navbar/Base.vue';

export default {
    AccordionBase,
    AccordionItem,
    ModalBase,
    ModalHeader,
    ModalBody,
    ModalFooter,
    AlertBase,
    SliderBase,
    FormButtonBase,
    FormInputBase,
    FormInputText,
    FormSelectBase,
    NavbarBase
}
