# uk-select

## Props

<!-- @vuese:uk-select:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|select|-|`Object`|`true`|-|

<!-- @vuese:uk-select:props:end -->


