# uk-alert

Display success, warning and error messages. <br /><br /> `<uk-alert :class="class" :content="html" :animation="animation" :duration:="duration"/>`

## Props

<!-- @vuese:uk-alert:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|content|The HTML content of the alert|`String`|`true`|-|
|closable|-|`Boolean`|`false`|true|
|largeCloseIcon|-|`Boolean`|`false`|false|
|class|The class of the alert|`String`|`false`|-|
|animation|Fade out or hide directly|`Boolean`|`false`|true|
|duration|Animation duration in milliseconds.|`Number`|`false`|150|

<!-- @vuese:uk-alert:props:end -->


