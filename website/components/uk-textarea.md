# uk-textarea

## Props

<!-- @vuese:uk-textarea:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|textarea|-|`Object`|`true`|-|

<!-- @vuese:uk-textarea:props:end -->


