# uk-modal-body

## Props

<!-- @vuese:uk-modal-body:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|content|-|`Object`|`true`|-|

<!-- @vuese:uk-modal-body:props:end -->


