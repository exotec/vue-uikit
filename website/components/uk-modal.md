# uk-modal

## Props

<!-- @vuese:uk-modal:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|modal|-|`Object`|`true`|-|

<!-- @vuese:uk-modal:props:end -->


