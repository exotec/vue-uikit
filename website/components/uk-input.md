# uk-input

Easily create nice looking buttons, which come in different styles. <br /><br /> `<uk-button :label="label" :class="class" :size="size" :name="name" :icon="icon" :iconPosition="iconPosition" :disabled="disabled"/>`

## Props

<!-- @vuese:uk-input:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|input|-|`Object`|`true`|-|

<!-- @vuese:uk-input:props:end -->


