# uk-button

Easily create nice looking buttons, which come in different styles. <br /><br /> `<uk-button :label="label" :class="class" :size="size" :name="name" :icon="icon" :iconPosition="iconPosition" :disabled="disabled"/>`

## Props

<!-- @vuese:uk-button:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|label|The label of the button|`String`|`true`|-|
|class|The CSS class of the button|`String`|`false`|-|
|name|The name of the button|`String`|`false`|button|
|disabled|Disable the button|`Boolean`|`false`|false|
|icon|The icon of the button|`String`|`false`|-|
|iconPosition|The icon position|`String`|`false`|left|
|size|The size of the button|`String`|`false`|-|

<!-- @vuese:uk-button:props:end -->


