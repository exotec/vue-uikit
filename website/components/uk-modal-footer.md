# uk-modal-footer

## Props

<!-- @vuese:uk-modal-footer:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|content|-|`Object`|`true`|-|

<!-- @vuese:uk-modal-footer:props:end -->


