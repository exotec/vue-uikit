# uk-accordion

Create a list of items that can be shown individually by clicking an item's header. <br /><br /> `<uk-accordion :items="items" :multiple="multiple" :collapsible="collapsible" :transition="transition" :duration="duration"/>`

## Props

<!-- @vuese:uk-accordion:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|items|Array of item objects<br /> [@see &lt;uk-accordion-item&gt;](/components/uk-accordion-item).|`Array`|`true`|-|
|multiple|Allow multiple open items|`Boolean`|`false`|false|
|collapsible|Allow all items to be closed|`Boolean`|`false`|true|
|transition|The transition to use when revealing items. Use keyword for [@see easing functions](https://developer.mozilla.org/en-US/docs/Web/CSS/easing-function#values)|`String`|`false`|ease|
|duration|Animation duration in milliseconds|`Number`|`false`|200|
|active|Index of the element to open initially|`Number`|`false`|-|

<!-- @vuese:uk-accordion:props:end -->


