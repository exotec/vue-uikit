# uk-modal-header

## Props

<!-- @vuese:uk-modal-header:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|content|-|`Object`|`false`|-|

<!-- @vuese:uk-modal-header:props:end -->


