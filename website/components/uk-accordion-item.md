# uk-accordion-item

The item of accordion <br /><br /> `   <uk-accordion-item :title="item.title" :class="item.class" :content="item.content" :open="item.open"/>`

## Props

<!-- @vuese:uk-accordion-item:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|title|The title of the item|`String`|`true`|-|
|class|The CSS class of the item|`String`|`false`|-|
|content|The HTML content of the item|`String`|`true`|-|
|open|Whether the item is open or not|`Boolean`|`false`|-|

<!-- @vuese:uk-accordion-item:props:end -->


